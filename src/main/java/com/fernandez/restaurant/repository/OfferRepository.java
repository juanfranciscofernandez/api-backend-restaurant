package com.fernandez.restaurant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fernandez.restaurant.model.Offer;

@Repository
public interface OfferRepository extends JpaRepository<Offer, Long> {

}
