package com.fernandez.restaurant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.fernandez.restaurant.model.Menu;

@Component
public interface MenuRepository extends JpaRepository<Menu, Long> {

}
