package com.fernandez.restaurant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fernandez.restaurant.model.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
	
}
