package com.fernandez.restaurant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fernandez.restaurant.model.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

}
