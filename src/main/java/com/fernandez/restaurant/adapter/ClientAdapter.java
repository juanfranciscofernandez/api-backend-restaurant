package com.fernandez.restaurant.adapter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fernandez.restaurant.dto.ClientDTO;
import com.fernandez.restaurant.model.Client;

@Component
public class ClientAdapter {

	public List<ClientDTO> convertModelList2DTO(List<Client> listClient) {

		List<ClientDTO> listClientDTO = new ArrayList<ClientDTO>();

		for (Client client : listClient) {
			ClientDTO clientDTO = new ClientDTO();
			clientDTO.setId(client.getId());
			clientDTO.setName(client.getName());
			clientDTO.setUsername(client.getUsername());
			listClientDTO.add(clientDTO);
		}

		return listClientDTO;

	}

	public Client convertDTO2Model(ClientDTO clientDTO) {
		Client client = new Client();
		client.setId(clientDTO.getId());
		client.setName(clientDTO.getName());
		client.setUsername(clientDTO.getUsername());
		return client;
	}

	public ClientDTO convertModel2DTO(Client client) {
		ClientDTO clientDTO = new ClientDTO();
		clientDTO.setId(client.getId());
		clientDTO.setName(client.getName());
		clientDTO.setUsername(client.getUsername());
		return clientDTO;
	}

}
