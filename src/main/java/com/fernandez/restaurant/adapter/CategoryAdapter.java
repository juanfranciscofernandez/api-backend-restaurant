package com.fernandez.restaurant.adapter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fernandez.restaurant.dto.CategoryDTO;
import com.fernandez.restaurant.model.Category;

@Component	
public class CategoryAdapter {

	public List<CategoryDTO> convertModelList2DTO(List<Category> listCategory) {
		
		List<CategoryDTO> listCategoryDTO = new ArrayList<CategoryDTO>();
		
		for (Category category : listCategory) {
			CategoryDTO categoryDTO = new CategoryDTO();
			categoryDTO.setId(category.getId());
			categoryDTO.setName(category.getName());
			listCategoryDTO.add(categoryDTO);
		}
		
		return listCategoryDTO;

	}
	
	public Category convertDTO2Model(CategoryDTO categoryDTO) {
		Category category = new Category();
		category.setId(categoryDTO.getId());
		category.setName(categoryDTO.getName());
		return category;
	}
	
	public CategoryDTO convertModel2DTO(Category category) {
		CategoryDTO categoryDTO = new CategoryDTO();
		categoryDTO.setId(category.getId());
		categoryDTO.setName(category.getName());
		return categoryDTO;
	}
	
}
