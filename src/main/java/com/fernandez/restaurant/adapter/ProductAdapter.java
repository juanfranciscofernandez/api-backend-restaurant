package com.fernandez.restaurant.adapter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fernandez.restaurant.dto.ProductDTO;
import com.fernandez.restaurant.model.Product;

@Component
public class ProductAdapter {
	
	public List<ProductDTO> convertModelList2DTO(List<Product> listProduct) {

		List<ProductDTO> listProductDTO = new ArrayList<ProductDTO>();

		for (Product product : listProduct) {
			ProductDTO productDTO = new ProductDTO();
			productDTO.setId(product.getId());
			productDTO.setName(product.getName());
			productDTO.setDescription(product.getDescription());
			productDTO.setImage(product.getImage());
			productDTO.setStock(product.getStock());
			listProductDTO.add(productDTO);
		}

		return listProductDTO;

	}
	
	public Product convertDTO2Model(ProductDTO prouctDTO) {
		Product product = new Product();
		product.setId(prouctDTO.getId());
		product.setName(prouctDTO.getName());
		product.setDescription(prouctDTO.getDescription());
		product.setStock(prouctDTO.getStock());
		product.setImage(prouctDTO.getImage());
		return product;
	}
	
	public ProductDTO convertModel2DTO(Product products) {
		ProductDTO productDTO = new ProductDTO();
		productDTO.setId(products.getId());
		productDTO.setName(products.getName());
		productDTO.setDescription(products.getDescription());
		productDTO.setImage(products.getImage());
		productDTO.setStock(products.getStock());
		return productDTO;
	}
	
}
