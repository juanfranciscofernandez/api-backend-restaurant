package com.fernandez.restaurant.adapter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fernandez.restaurant.dto.DishDTO;
import com.fernandez.restaurant.model.Dish;

@Component
public class DishAdapter {

	public List<DishDTO> convertModelList2DTO(List<Dish> listDishes) {

		List<DishDTO> listDishesDTO = new ArrayList<DishDTO>();

		for (Dish dishes : listDishes) {
			
			DishDTO dishesDTO = new DishDTO();
			dishesDTO.setId(dishes.getId());
			dishesDTO.setName(dishes.getName());
			dishesDTO.setDescription(dishes.getDescription());
			dishesDTO.setImage(dishes.getImage());
			listDishesDTO.add(dishesDTO);

		}
		

		return listDishesDTO;

	} 
	
	public DishDTO convertModel2DTO(Dish dish) {
		DishDTO dishDTO = new DishDTO();
		dishDTO.setId(dish.getId());
		dishDTO.setName(dish.getName());
		dishDTO.setDescription(dish.getDescription());
		dishDTO.setImage(dish.getImage());
		return dishDTO;
	}
	
	public Dish convertDTO2Model(DishDTO dishDTO) {
		Dish dish = new Dish();
		dish.setId(dishDTO.getId());
		dish.setName(dishDTO.getName());
		dish.setDescription(dishDTO.getDescription());
		dish.setImage(dishDTO.getImage());
		return dish;
	}
}
