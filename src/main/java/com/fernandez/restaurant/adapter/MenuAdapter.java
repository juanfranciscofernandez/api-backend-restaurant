package com.fernandez.restaurant.adapter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.restaurant.dto.DishDTO;
import com.fernandez.restaurant.dto.MenuDTO;
import com.fernandez.restaurant.model.Dish;
import com.fernandez.restaurant.model.Menu;

@Component
public class MenuAdapter {
	
	@Autowired
	private MealAdapter mealAdapter;
	
	public List<MenuDTO> convertModelList2DTO(List<Menu> listMenu) {
		List<MenuDTO> menuList = new ArrayList<MenuDTO>();
		for (Menu menu : listMenu) {
			MenuDTO menuDTO = new MenuDTO();
			menuDTO.setId(menu.getId());
			menuDTO.setName(menu.getName());
			menuDTO.setMealDTO(mealAdapter.convertModel2DTO(menu.getMeals()));
			
			ArrayList<DishDTO> dishDTOList = new ArrayList<DishDTO>();
			
			for (Dish dishes : menu.getdishes()) {
				DishDTO dishDTO = new DishDTO();
				dishDTO.setId(dishes.getId());
				dishDTO.setName(dishes.getName());
				dishDTOList.add(dishDTO);
			}
			
			menuList.add(menuDTO);
			
			menuDTO.setDishDTOList(dishDTOList);
		}
		return menuList;

	}

	public MenuDTO convertModel2DTO(Menu menu) {
		MenuDTO menuDTO = new MenuDTO();
		menuDTO.setId(menu.getId());
		menuDTO.setName(menu.getName());
		menuDTO.setMealDTO(mealAdapter.convertModel2DTO(menu.getMeals()));
		
		ArrayList<DishDTO> dishDTOList = new ArrayList<DishDTO>();
		
		for (Dish dishes : menu.getdishes()) {
			DishDTO dishDTO = new DishDTO();
			dishDTO.setId(dishes.getId());
			dishDTO.setName(dishes.getName()); 
			dishDTOList.add(dishDTO);
		}
		
		menuDTO.setDishDTOList(dishDTOList);
		
		return menuDTO;
	}

	
}
