package com.fernandez.restaurant.adapter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fernandez.restaurant.dto.OfferDTO;
import com.fernandez.restaurant.model.Offer;

@Component
public class OfferAdapter {

	public List<OfferDTO> convertModelList2DTO(List<Offer> listOffer) {
		
		List<OfferDTO> listOfferDTO = new ArrayList<OfferDTO>();
		
		for (Offer offer : listOffer) {
			OfferDTO offerDTO = new OfferDTO();
			offerDTO.setId(offer.getId());
			offerDTO.setName(offer.getName());
			listOfferDTO.add(offerDTO);
		}
		
		return listOfferDTO;

	}
	
	public Offer convertDTO2Model(OfferDTO offerDTO) {
		Offer offer = new Offer();
		offer.setId(offerDTO.getId());
		offer.setName(offerDTO.getName());
		return offer;
	}
	
	public OfferDTO convertModel2DTO(Offer offer) {
		OfferDTO offerDTO = new OfferDTO();
		offerDTO.setId(offer.getId());
		offerDTO.setName(offer.getName());
		return offerDTO;
	}
}
