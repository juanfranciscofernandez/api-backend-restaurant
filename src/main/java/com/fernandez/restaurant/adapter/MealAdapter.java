package com.fernandez.restaurant.adapter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fernandez.restaurant.dto.MealDTO;
import com.fernandez.restaurant.dto.MenuDTO;
import com.fernandez.restaurant.model.Meals;
import com.fernandez.restaurant.model.Menu;

@Component
public class MealAdapter {

	public List<MealDTO> convertModelList2DTO(List<Meals> listMeals) {

		List<MealDTO> listMealsDTO = new ArrayList<MealDTO>();

		for (Meals meals : listMeals) {
			
			MealDTO mealDTO = new MealDTO();
			mealDTO.setId(meals.getId());
			mealDTO.setName(meals.getName());
			
			List<MenuDTO> menuDTOList = new ArrayList<MenuDTO>();
			
			for (Menu menu : meals.getListMenu()) {
				
				MenuDTO menuDTO = new MenuDTO();
				menuDTO.setId(menu.getId());
				menuDTO.setName(menu.getName());
				menuDTOList.add(menuDTO);

			}
			
			mealDTO.setMenuDTOList(menuDTOList);
			listMealsDTO.add(mealDTO);
		}

		return listMealsDTO;

	}
	
	public Meals convertDTO2Model(MealDTO mealDTO) {
		Meals meal = new Meals();
		meal.setId(mealDTO.getId());
		meal.setName(mealDTO.getName());
		return meal;
	}
	
	public MealDTO convertModel2DTO(Meals meals) {
		MealDTO mealDTO = new MealDTO();
		mealDTO.setId(meals.getId());
		mealDTO.setName(meals.getName());
		return mealDTO;
	}
}
