package com.fernandez.restaurant.methods;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.restaurant.adapter.OfferAdapter;
import com.fernandez.restaurant.dto.OfferDTO;
import com.fernandez.restaurant.model.Offer;
import com.fernandez.restaurant.services.OfferService;

@Component
public class OffersMethods {

	@Autowired
	private OfferAdapter offerAdapter;

	@Autowired
	private OfferService offerService;

	public List<OfferDTO> retreiveAllOffers() {
		return offerAdapter.convertModelList2DTO(offerService.retreiveListOffers());
	}
	
	public OfferDTO retreiveOffer(long offerId) {
		if(checkIfOfferExists(offerId)) {
			return offerAdapter.convertModel2DTO(offerService.retreiveOffer(offerId));
		}else {
			return null;	
		}
		
	}
	
	public OfferDTO create(OfferDTO offerDTO) {
		Offer offer =offerAdapter.convertDTO2Model(offerDTO);
		return offerAdapter.convertModel2DTO(offerService.create(offer));		
	}
	
	public OfferDTO update(OfferDTO offerDTO) {		
		if(checkIfOfferExists(offerDTO.getId())) {
			return offerAdapter.convertModel2DTO(offerService.updateUser(offerAdapter.convertDTO2Model(offerDTO)));	
		}else {
			return null;	
		}
		
	}
	
	public String deleteOffer(long offerId) {
		String message = new String();
		if(checkIfOfferExists(offerId)) {
			offerService.deleteById(offerId);
			return message = "The offer with id " +offerId + "has been delete correctly";
		}else {
			return message = "The offer with id "+ offerId +" doesn't exists";
		}
	}
	
	public boolean checkIfOfferExists(long offerId) {
		Optional<Offer> offer = offerService.checkIfOfferExists(offerId);
		if(offer.isPresent()) {
			return true;
		}else {
			return false;
		}
		
	}
	

}
