package com.fernandez.restaurant.methods;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.restaurant.adapter.MealAdapter;
import com.fernandez.restaurant.dto.MealDTO;
import com.fernandez.restaurant.dto.OfferDTO;
import com.fernandez.restaurant.model.Meals;
import com.fernandez.restaurant.services.MealService;

@Component
public class MealMethods {
	
	@Autowired
	private MealService mealService;
	
	@Autowired
	private MealAdapter mealAdapter;
	
	public MealDTO create(MealDTO mealDTO) {
		Meals meal = mealAdapter.convertDTO2Model(mealDTO);
		return mealAdapter.convertModel2DTO(mealService.create(meal));		
	}
	
	public List<MealDTO> retreiveAllMeals() {
		return mealAdapter.convertModelList2DTO(mealService.retreiveListMeals());
	}
	
	public MealDTO retreiveMeal(long mealId) {
		if(checkIfMealExists(mealId)) {
			return mealAdapter.convertModel2DTO(mealService.retreiveMeals(mealId));
		}else {
			return null;	
		}
		
	}
	
	public String deleteMeals(long offerId) {
		String message = new String();
		if(checkIfMealExists(offerId)) {
			mealService.deleteById(offerId);
			return message = "The offer with id " +offerId + "has been delete correctly";
		}else {
			return message = "The offer with id "+ offerId +" doesn't exists";
		}
	}
	

	public boolean checkIfMealExists(long mealId) {
		Optional<Meals> meal = mealService.checkIfMealsExists(mealId);
		if(meal.isPresent()) {
			return true;
		}else {
			return false;
		}
		
	}
	
	public MealDTO update(MealDTO mealDTO) {		
		if(checkIfMealExists(mealDTO.getId())) {
			return mealAdapter.convertModel2DTO(mealService.updateMeals(mealAdapter.convertDTO2Model(mealDTO)));	
		}else {
			return null;	
		}
		
	}
	
}
