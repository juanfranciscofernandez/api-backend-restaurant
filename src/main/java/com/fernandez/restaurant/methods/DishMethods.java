package com.fernandez.restaurant.methods;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.restaurant.adapter.DishAdapter;
import com.fernandez.restaurant.dto.DishDTO;
import com.fernandez.restaurant.model.Dish;
import com.fernandez.restaurant.services.DishesServices;

@Component
public class DishMethods {
	
	@Autowired
	private DishesServices dishesService;
	
	@Autowired
	private DishAdapter dishesAdapter;
	
	public List<DishDTO> retreiveAllDishes() {
		return dishesAdapter.convertModelList2DTO(dishesService.retreiveListDishes());
	}
	
	public DishDTO retreiveDish(long dishId) {
		if(checkIfDishExists(dishId)) {
			return dishesAdapter.convertModel2DTO(dishesService.retreive(dishId));
		}else {
			return null;	
		}
		
	}
	
	public boolean checkIfDishExists(long dishesId) {
		Optional<Dish> meal = dishesService.checkIfDishesExists(dishesId);
		if(meal.isPresent()) {
			return true;
		}else {
			return false;
		}
		
	}

	public String deleteDishes(long dishId) {
		String message = new String();
		if(checkIfDishExists(dishId)) {
			dishesService.deleteById(dishId);
			return message = "The offer with id " +dishId + "has been delete correctly";
		}else {
			return message = "The offer with id "+ dishId +" doesn't exists";
		}
	}

	public DishDTO create(DishDTO dishDTO) {
		Dish meal = dishesAdapter.convertDTO2Model(dishDTO);
		return dishesAdapter.convertModel2DTO(dishesService.create(meal));	
	}

	public DishDTO update(DishDTO dishDTO) {		
		if(checkIfDishExists(dishDTO.getId())) {
			return dishesAdapter.convertModel2DTO(dishesService.update(dishesAdapter.convertDTO2Model(dishDTO)));	
		}else {
			return null;	
		}
		
	}
}
