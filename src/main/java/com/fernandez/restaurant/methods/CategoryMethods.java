package com.fernandez.restaurant.methods;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.restaurant.adapter.CategoryAdapter;
import com.fernandez.restaurant.dto.CategoryDTO;
import com.fernandez.restaurant.model.Category;
import com.fernandez.restaurant.model.Meals;
import com.fernandez.restaurant.services.CategoryService;

@Component
public class CategoryMethods {
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private CategoryAdapter categoryAdapter;
	
	public CategoryDTO create(CategoryDTO categoryDTO) {
		Category category = categoryAdapter.convertDTO2Model(categoryDTO);
		return categoryAdapter.convertModel2DTO(categoryService.create(category));		
	}
	
	public List<CategoryDTO> retreiveAllCategories() {
		return categoryAdapter.convertModelList2DTO(categoryService.retreiveListOfCategories());
	}
	
	public CategoryDTO retreiveCategory(long categoryId) {
		if(checkIfCategoryExists(categoryId)) {
			return categoryAdapter.convertModel2DTO(categoryService.retreiveCategory(categoryId));
		}else {
			return null;	
		}
		
	}
	
	public String deleteCategory(long categoryId) {
		String message = new String();
		if(checkIfCategoryExists(categoryId)) {
			categoryService.deleteById(categoryId);
			return message = "The offer with id " +categoryId + "has been delete correctly";
		}else {
			return message = "The offer with id "+ categoryId +" doesn't exists";
		}
	}
	
	public CategoryDTO updateCategory(CategoryDTO categoryDTO) {		
		if(checkIfCategoryExists(categoryDTO.getId())) {
			return categoryAdapter.convertModel2DTO(categoryService.updateCategory(categoryAdapter.convertDTO2Model(categoryDTO)));	
		}else {
			return null;	
		}
		
	}
	

	public boolean checkIfCategoryExists(long categoryId) {
		Optional<Category> meal = categoryService.checkIfCategoryExists(categoryId);
		if(meal.isPresent()) {
			return true;
		}else {
			return false;
		}
		
	}
	
	
}
