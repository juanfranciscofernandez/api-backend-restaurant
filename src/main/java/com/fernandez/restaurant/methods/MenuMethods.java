package com.fernandez.restaurant.methods;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.restaurant.adapter.MenuAdapter;
import com.fernandez.restaurant.dto.MenuDTO;
import com.fernandez.restaurant.model.Menu;
import com.fernandez.restaurant.services.MenuService;

@Component
public class MenuMethods {
	
	@Autowired
	private MenuService menuService;
	
	@Autowired
	private MenuAdapter menuAdapter;
	
	public List<MenuDTO> retreiveAllMenus() {
		return menuAdapter.convertModelList2DTO(menuService.retreiveListMenu());
	}
	
	
	public MenuDTO retreiveMenu(long menuId) {
		if(checkIfMenuExists(menuId)) {
			return menuAdapter.convertModel2DTO(menuService.retreiveMenu(menuId));
		}else {
			return null;	
		}
		
	}
	
	public boolean checkIfMenuExists(long menuId) {
		Optional<Menu> offer = menuService.checkIfMenuExists(menuId);
		if(offer.isPresent()) {
			return true;
		}else {
			return false;
		}
		
	}
	
}
