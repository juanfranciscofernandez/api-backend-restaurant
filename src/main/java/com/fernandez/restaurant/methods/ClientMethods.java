package com.fernandez.restaurant.methods;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.restaurant.adapter.ClientAdapter;
import com.fernandez.restaurant.dto.ClientDTO;
import com.fernandez.restaurant.model.Client;
import com.fernandez.restaurant.services.ClientService;

@Component
public class ClientMethods {
	
	@Autowired
	private ClientService clientService;
	
	@Autowired
	private ClientAdapter clientAdapter;
	
	public ClientDTO create(ClientDTO clientDTO) {
		Client client = clientAdapter.convertDTO2Model(clientDTO);
		return clientAdapter.convertModel2DTO(clientService.create(client));		
	}
	
	public List<ClientDTO> retreiveAllClients() {
		return clientAdapter.convertModelList2DTO(clientService.retreiveListOfClient());
	}
	
	public ClientDTO retreiveClient(long clientID) {
		if(checkIfClientExists(clientID)) {
			return clientAdapter.convertModel2DTO(clientService.retreiveClient(clientID));
		}else {
			return null;	
		}
		
	}
	
	public String deleteClient(long clientId) {
		String message = new String();
		if(checkIfClientExists(clientId)) {
			clientService.deleteById(clientId);
			return message = "The client with id " +clientId + "has been delete correctly";
		}else {
			return message = "The client with id "+ clientId +" doesn't exists";
		}
	}
	
	public ClientDTO updateClient(ClientDTO clientDTO) {		
		if(checkIfClientExists(clientDTO.getId())) {
			return clientAdapter.convertModel2DTO(clientService.updateClient(clientAdapter.convertDTO2Model(clientDTO)));	
		}else {
			return null;	
		}
		
	}
	

	public boolean checkIfClientExists(long clientId) {
		Optional<Client> meal = clientService.checkIfClientExists(clientId);
		if(meal.isPresent()) {
			return true;
		}else {
			return false;
		}
		
	}
	
	
}
