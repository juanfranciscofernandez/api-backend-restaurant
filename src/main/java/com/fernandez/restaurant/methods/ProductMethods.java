package com.fernandez.restaurant.methods;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.restaurant.adapter.ProductAdapter;
import com.fernandez.restaurant.dto.ProductDTO;
import com.fernandez.restaurant.model.Product;
import com.fernandez.restaurant.services.ProductService;

@Component
public class ProductMethods {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ProductAdapter productAdapter;
	
	public ProductDTO create(ProductDTO productDTO) {
		Product product = productAdapter.convertDTO2Model(productDTO);
		return productAdapter.convertModel2DTO(productService.create(product));		
	}
	
	public List<ProductDTO> retreiveAllProducts() {
		return productAdapter.convertModelList2DTO(productService.retreiveListOProducts());
	}
	
	public ProductDTO retreiveProduct(long productId) {
		if(checkIfProductExists(productId)) {
			return productAdapter.convertModel2DTO(productService.retreiveProduct(productId));
		}else {
			return null;	
		}
		
	}
	
	public String deleteProducts(long offerId) {
		String message = new String();
		if(checkIfProductExists(offerId)) {
			productService.deleteById(offerId);
			return message = "The product with id " +offerId + "has been delete correctly";
		}else {
			return message = "The product with id "+ offerId +" doesn't exists";
		}
	}
	

	public boolean checkIfProductExists(long productId) {
		Optional<Product> meal = productService.checkIfProductExists(productId);
		if(meal.isPresent()) {
			return true;
		}else {
			return false;
		}
		
	}
	
	public ProductDTO update(ProductDTO mealDTO) {		
		if(checkIfProductExists(mealDTO.getId())) {
			return productAdapter.convertModel2DTO(productService.updateProduct(productAdapter.convertDTO2Model(mealDTO)));	
		}else {
			return null;	
		}
		
	}
	
}
