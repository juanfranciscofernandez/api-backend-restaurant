package com.fernandez.restaurant.utils;

import org.springframework.http.HttpStatus;

public class InfoDTO {
	
	private String description;

	private HttpStatus status;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "InfoDTO [description=" + description + "]";
	}
	
	
	
}
