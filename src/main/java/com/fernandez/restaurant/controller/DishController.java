package com.fernandez.restaurant.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fernandez.restaurant.dto.DishDTO;
import com.fernandez.restaurant.methods.DishMethods;
import com.fernandez.restaurant.utils.InfoDTO;

@RestController
@RequestMapping("/api/")
public class DishController {
	
	Logger logger = LoggerFactory.getLogger(DishController.class);

	@Autowired
	private DishMethods dishMethods;
	
	@GetMapping(value = "/v1/dishes")
	public ResponseEntity<?> retreiveAllDishes() {
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName());

		List<DishDTO> listDishes = dishMethods.retreiveAllDishes();
		
		if (listDishes != null && !listDishes.isEmpty()) {
			return new ResponseEntity<List<DishDTO>>(listDishes,(listDishes == null || listDishes.isEmpty()) ? HttpStatus.NO_CONTENT : HttpStatus.OK);
		} else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The list are empty");
			infoDTO.setStatus(HttpStatus.NO_CONTENT);
			return ResponseEntity.ok(infoDTO);
		}
	}
	
	@GetMapping(value = "/v1/dish/{dishesId}")
	public ResponseEntity<?> retriveDish(@PathVariable("dishesId") long dishesId){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName());
	
		if(dishMethods.checkIfDishExists(dishesId)) {
			return ResponseEntity.ok(dishMethods.retreiveDish(dishesId));
		}else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The product with id" + dishesId + " doesn't exists");
			infoDTO.setStatus(HttpStatus.NOT_FOUND);
			return ResponseEntity.ok(infoDTO);
		}
		
	}
	
	@DeleteMapping(value = "/v1/dish/{dishId}")
	public ResponseEntity<?> deleteDish(@PathVariable("dishId") long dishId){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName() + "--- ID ---" + dishId );
		
		InfoDTO infoDTO = new InfoDTO();
		infoDTO.setDescription(dishMethods.deleteDishes(dishId));
		infoDTO.setStatus(HttpStatus.NOT_FOUND);
		return ResponseEntity.ok(infoDTO);
	}
	
	@PostMapping(value = "/v1/dish")
	public ResponseEntity<DishDTO> createDish(@RequestBody DishDTO dishDTO ){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName() + "--- ID ---" + dishDTO );
				 
		return ResponseEntity.ok(dishMethods.create(dishDTO));
		
	}
	
	@PutMapping(value = "/v1/dish/{dishId}")	
	public ResponseEntity<?> updateDish(@PathVariable("dishId") long dishId , @RequestBody DishDTO dishDTO){
		if(dishMethods.checkIfDishExists(dishId)) {
			dishDTO.setId(dishId);
			return ResponseEntity.ok(dishMethods.update(dishDTO));
		}else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The offer with id" + dishId + " doesn't exists");
			infoDTO.setStatus(HttpStatus.NOT_FOUND);
			return ResponseEntity.ok(infoDTO);
		}
	
	}
		
	
}
