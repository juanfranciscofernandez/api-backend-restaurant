package com.fernandez.restaurant.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fernandez.restaurant.dto.CategoryDTO;
import com.fernandez.restaurant.methods.CategoryMethods;
import com.fernandez.restaurant.utils.InfoDTO;

@RestController
@RequestMapping("/api/")
public class CategoryController {
	
	Logger logger = LoggerFactory.getLogger(CategoryController.class);

	@Autowired
	private CategoryMethods categoryMethods;
	
	@GetMapping(value = "/v1/categories")
	public ResponseEntity<?> retreiveAllCategories() {
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName());

		List<CategoryDTO> listCategories = categoryMethods.retreiveAllCategories();
		
		if (listCategories != null && !listCategories.isEmpty()) {
			return new ResponseEntity<List<CategoryDTO>>(listCategories,(listCategories == null || listCategories.isEmpty()) ? HttpStatus.NO_CONTENT : HttpStatus.OK);
		} else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The list are empty");
			infoDTO.setStatus(HttpStatus.NO_CONTENT);
			return ResponseEntity.ok(infoDTO);
		}
	}
	
	@GetMapping(value = "/v1/category/{categoryId}")
	public ResponseEntity<?> retreiveCategory(@PathVariable("categoryId") long categoryId){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName());
	
		if(categoryMethods.checkIfCategoryExists(categoryId)) {
			return ResponseEntity.ok(categoryMethods.retreiveCategory(categoryId));
		}else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The category with id" + categoryId + " doesn't exists");
			infoDTO.setStatus(HttpStatus.NOT_FOUND);
			return ResponseEntity.ok(infoDTO);
		}
		
	}
	
	@DeleteMapping(value = "/v1/category/{categoryId}")
	public ResponseEntity<?> deleteProduct(@PathVariable("categoryId") long categoryId){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName() + "--- ID ---" + categoryId );
		
		InfoDTO infoDTO = new InfoDTO();
		infoDTO.setDescription(categoryMethods.deleteCategory(categoryId));
		infoDTO.setStatus(HttpStatus.NOT_FOUND);
		return ResponseEntity.ok(infoDTO);
	}
	
	@PostMapping(value = "/v1/category")
	public ResponseEntity<CategoryDTO> createProduct(@RequestBody CategoryDTO categoryDTO ){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName() + "--- ID ---" + categoryDTO );
				 
		return ResponseEntity.ok(categoryMethods.create(categoryDTO));
		
	}
	
	@PutMapping(value = "/v1/category/{categoryId}")	
	public ResponseEntity<?> updateOffer(@PathVariable("categoryId") long categoryId , @RequestBody CategoryDTO categoryDTO){
		if(categoryMethods.checkIfCategoryExists(categoryId)) {
			categoryDTO.setId(categoryId);
			return ResponseEntity.ok(categoryMethods.updateCategory(categoryDTO));
		}else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The category with id" + categoryId + " doesn't exists");
			infoDTO.setStatus(HttpStatus.NOT_FOUND);
			return ResponseEntity.ok(infoDTO);
		}
	
	}
		
	
}
