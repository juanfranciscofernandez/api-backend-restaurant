package com.fernandez.restaurant.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fernandez.restaurant.dto.ClientDTO;
import com.fernandez.restaurant.methods.ClientMethods;
import com.fernandez.restaurant.utils.InfoDTO;

@RestController
@RequestMapping("/api/")
public class ClientController {
	
	Logger logger = LoggerFactory.getLogger(ClientController.class);

	@Autowired
	private ClientMethods clientMethods;
	
	@GetMapping(value = "/v1/clients")
	public ResponseEntity<?> retreiveAllClients() {
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName());

		List<ClientDTO> listClients = clientMethods.retreiveAllClients();
		
		if (listClients != null && !listClients.isEmpty()) {
			return new ResponseEntity<List<ClientDTO>>(listClients,(listClients == null || listClients.isEmpty()) ? HttpStatus.NO_CONTENT : HttpStatus.OK);
		} else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The list are empty");
			infoDTO.setStatus(HttpStatus.NO_CONTENT);
			return ResponseEntity.ok(infoDTO);
		}
	}
	
	@GetMapping(value = "/v1/client/{clientId}")
	public ResponseEntity<?> retreiveClient(@PathVariable("clientId") long clientId){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName());
	
		if(clientMethods.checkIfClientExists(clientId)) {
			return ResponseEntity.ok(clientMethods.retreiveClient(clientId));
		}else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The client with id" + clientId + " doesn't exists");
			infoDTO.setStatus(HttpStatus.NOT_FOUND);
			return ResponseEntity.ok(infoDTO);
		}
		
	}
	
	@DeleteMapping(value = "/v1/client/{clientId}")
	public ResponseEntity<?> deleteClient(@PathVariable("clientId") long clientId){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName() + "--- ID ---" + clientId );
		
		InfoDTO infoDTO = new InfoDTO();
		infoDTO.setDescription(clientMethods.deleteClient(clientId));
		infoDTO.setStatus(HttpStatus.NOT_FOUND);
		return ResponseEntity.ok(infoDTO);
	}
	
	@PostMapping(value = "/v1/client")
	public ResponseEntity<ClientDTO> createProduct(@RequestBody ClientDTO clientDTO ){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName() + "--- ID ---" + clientDTO );
				 
		return ResponseEntity.ok(clientMethods.create(clientDTO));
		
	}
	
	@PutMapping(value = "/v1/client/{clientId}")	
	public ResponseEntity<?> updateOffer(@PathVariable("clientId") long clientId , @RequestBody ClientDTO clientDTO){
		if(clientMethods.checkIfClientExists(clientId)) {
			clientDTO.setId(clientId);
			return ResponseEntity.ok(clientMethods.updateClient(clientDTO));
		}else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The category with id" + clientId + " doesn't exists");
			infoDTO.setStatus(HttpStatus.NOT_FOUND);
			return ResponseEntity.ok(infoDTO);
		}
	
	}
		
	
}
