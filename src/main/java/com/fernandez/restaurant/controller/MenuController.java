package com.fernandez.restaurant.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fernandez.restaurant.dto.MenuDTO;
import com.fernandez.restaurant.dto.OfferDTO;
import com.fernandez.restaurant.methods.MenuMethods;
import com.fernandez.restaurant.utils.InfoDTO;

@RestController
@RequestMapping("/api/")
public class MenuController {

	Logger logger = LoggerFactory.getLogger(MenuController.class);
	
	@Autowired
	private MenuMethods menuMethods;
	
	@GetMapping(value = "/v1/menu")
	public ResponseEntity<?> retreiveAllMenu() {
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName());

		List<MenuDTO> listMenu = menuMethods.retreiveAllMenus();
		
		if (listMenu!= null && !listMenu.isEmpty()) {
			return new ResponseEntity<List<MenuDTO>>(listMenu,(listMenu == null || listMenu.isEmpty()) ? HttpStatus.NO_CONTENT : HttpStatus.OK);
		} else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The list are empty");
			infoDTO.setStatus(HttpStatus.NO_CONTENT);
			return ResponseEntity.ok(infoDTO);
		}

	}
	
	@GetMapping(value = "/v1/menu/{menuId}")
	public ResponseEntity<?> retreiveMenu(@PathVariable("menuId") long menuId){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName());
	
		if(menuMethods.checkIfMenuExists(menuId)) {
			return ResponseEntity.ok(menuMethods.retreiveMenu(menuId));
		}else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The meal with id" + menuId + " doesn't exists");
			infoDTO.setStatus(HttpStatus.NOT_FOUND);
			return ResponseEntity.ok(infoDTO);
		}
		
	}
	
	
}
