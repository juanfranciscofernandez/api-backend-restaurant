package com.fernandez.restaurant.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fernandez.restaurant.dto.ProductDTO;
import com.fernandez.restaurant.methods.ProductMethods;
import com.fernandez.restaurant.utils.InfoDTO;

@RestController
@RequestMapping("/api/")
public class ProductController {
	
	Logger logger = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	private ProductMethods productsMethods;
	
	@GetMapping(value = "/v1/products")
	public ResponseEntity<?> retreiveAllProducts() {
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName());

		List<ProductDTO> listProducts = productsMethods.retreiveAllProducts();
		
		if (listProducts != null && !listProducts.isEmpty()) {
			return new ResponseEntity<List<ProductDTO>>(listProducts,(listProducts == null || listProducts.isEmpty()) ? HttpStatus.NO_CONTENT : HttpStatus.OK);
		} else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The list are empty");
			infoDTO.setStatus(HttpStatus.NO_CONTENT);
			return ResponseEntity.ok(infoDTO);
		}
	}
	
	@GetMapping(value = "/v1/product/{productId}")
	public ResponseEntity<?> retreiveProduct(@PathVariable("productId") long mealId){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName());
	
		if(productsMethods.checkIfProductExists(mealId)) {
			return ResponseEntity.ok(productsMethods.retreiveProduct(mealId));
		}else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The product with id" + mealId + " doesn't exists");
			infoDTO.setStatus(HttpStatus.NOT_FOUND);
			return ResponseEntity.ok(infoDTO);
		}
		
	}
	
	@DeleteMapping(value = "/v1/product/{productId}")
	public ResponseEntity<?> deleteProduct(@PathVariable("productId") long productId){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName() + "--- ID ---" + productId );
		
		InfoDTO infoDTO = new InfoDTO();
		infoDTO.setDescription(productsMethods.deleteProducts(productId));
		infoDTO.setStatus(HttpStatus.NOT_FOUND);
		return ResponseEntity.ok(infoDTO);
	}
	
	@PostMapping(value = "/v1/product")
	public ResponseEntity<ProductDTO> createProduct(@RequestBody ProductDTO productDTO ){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName() + "--- ID ---" + productDTO );
				 
		return ResponseEntity.ok(productsMethods.create(productDTO));
		
	}
	
	@PutMapping(value = "/v1/product/{productId}")	
	public ResponseEntity<?> updateOffer(@PathVariable("productId") long productId , @RequestBody ProductDTO productDTO){
		if(productsMethods.checkIfProductExists(productId)) {
			productDTO.setId(productId);
			return ResponseEntity.ok(productsMethods.update(productDTO));
		}else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The offer with id" + productId + " doesn't exists");
			infoDTO.setStatus(HttpStatus.NOT_FOUND);
			return ResponseEntity.ok(infoDTO);
		}
	
	}
		
	
}
