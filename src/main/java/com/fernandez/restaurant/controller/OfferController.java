package com.fernandez.restaurant.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fernandez.restaurant.dto.OfferDTO;
import com.fernandez.restaurant.methods.OffersMethods;
import com.fernandez.restaurant.utils.InfoDTO;

@RestController
@RequestMapping("/api/")
public class OfferController {

	Logger logger = LoggerFactory.getLogger(OfferController.class);

	@Autowired
	private OffersMethods offersMethods;
		
	@GetMapping(value = "/v1/offers")
	public ResponseEntity<?> retreiveAllOffers() {
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName());

		List<OfferDTO> listOffers = offersMethods.retreiveAllOffers();
		
		if (listOffers!= null && !listOffers.isEmpty()) {
			return new ResponseEntity<List<OfferDTO>>(listOffers,(listOffers == null || listOffers.isEmpty()) ? HttpStatus.NO_CONTENT : HttpStatus.OK);
		} else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The list are empty");
			infoDTO.setStatus(HttpStatus.NO_CONTENT);
			return ResponseEntity.ok(infoDTO);
		}

	}
	
	@GetMapping(value = "/v1/offer/{offerId}")
	public ResponseEntity<?> retreiveOffer(@PathVariable("offerId") long offerId){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName());
	
		if(offersMethods.checkIfOfferExists(offerId)) {
			return ResponseEntity.ok(offersMethods.retreiveOffer(offerId));
		}else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The offer with id" + offerId + " doesn't exists");
			infoDTO.setStatus(HttpStatus.NOT_FOUND);
			return ResponseEntity.ok(infoDTO);
		}
		
	}
	
	@DeleteMapping(value = "/v1/offer/{offerId}")
	public ResponseEntity<?> deleteOffer(@PathVariable("offerId") long offerId){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName() + "--- ID ---" + offerId );
		
		InfoDTO infoDTO = new InfoDTO();
		infoDTO.setDescription(offersMethods.deleteOffer(offerId));
		infoDTO.setStatus(HttpStatus.NOT_FOUND);
		return ResponseEntity.ok(infoDTO);
	}
	
	@PostMapping(value = "/v1/offer")
	public ResponseEntity<OfferDTO> createOffer(@RequestBody OfferDTO offerDTO ){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName() + "--- ID ---" + offerDTO );
				 
		return ResponseEntity.ok(offersMethods.create(offerDTO));
		
	}
	
	@PutMapping(value = "/v1/offer/{offerId}")	
	public ResponseEntity<?> updateOffer(@PathVariable("offerId") long offerId , @RequestBody OfferDTO offerDTO){
		if(offersMethods.checkIfOfferExists(offerId)) {
			offerDTO.setId(offerId);
			return ResponseEntity.ok(offersMethods.update(offerDTO));
		}else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The offer with id" + offerId + " doesn't exists");
			infoDTO.setStatus(HttpStatus.NOT_FOUND);
			return ResponseEntity.ok(infoDTO);
		}
	
	}

}
