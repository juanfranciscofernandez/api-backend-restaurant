package com.fernandez.restaurant.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fernandez.restaurant.dto.MealDTO;
import com.fernandez.restaurant.methods.MealMethods;
import com.fernandez.restaurant.utils.InfoDTO;

@RestController
@RequestMapping("/api/")
public class MealController {

	Logger logger = LoggerFactory.getLogger(MealController.class);
	
	@Autowired
	private MealMethods mealsMethods;
	
	@GetMapping(value = "/v1/meals")
	public ResponseEntity<?> retreiveAllMeals() {
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName());

		List<MealDTO> listMeals = mealsMethods.retreiveAllMeals();
		
		if (listMeals != null && !listMeals.isEmpty()) {
			return new ResponseEntity<List<MealDTO>>(listMeals,(listMeals == null || listMeals.isEmpty()) ? HttpStatus.NO_CONTENT : HttpStatus.OK);
		} else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The list are empty");
			infoDTO.setStatus(HttpStatus.NO_CONTENT);
			return ResponseEntity.ok(infoDTO);
		}
	}
	
	@GetMapping(value = "/v1/meal/{mealId}")
	public ResponseEntity<?> retreiveMeal(@PathVariable("mealId") long mealId){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName());
	
		if(mealsMethods.checkIfMealExists(mealId)) {
			return ResponseEntity.ok(mealsMethods.retreiveMeal(mealId));
		}else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The meal with id" + mealId + " doesn't exists");
			infoDTO.setStatus(HttpStatus.NOT_FOUND);
			return ResponseEntity.ok(infoDTO);
		}
		
	}
		
	@DeleteMapping(value = "/v1/meal/{mealId}")
	public ResponseEntity<?> deleteOffer(@PathVariable("mealId") long mealId){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName() + "--- ID ---" + mealId );
		
		InfoDTO infoDTO = new InfoDTO();
		infoDTO.setDescription(mealsMethods.deleteMeals(mealId));
		infoDTO.setStatus(HttpStatus.NOT_FOUND);
		return ResponseEntity.ok(infoDTO);
	}

	@PostMapping(value = "/v1/meal")
	public ResponseEntity<MealDTO> createMeal(@RequestBody MealDTO mealDTO ){
		
		logger.info("--- Retrieve name of class --- : " + this.getClass().getSimpleName() + " --- Method name --- : "
				+ new Object() {
				}.getClass().getEnclosingMethod().getName() + "--- ID ---" + mealsMethods );
				 
		return ResponseEntity.ok(mealsMethods.create(mealDTO));
		
	}
	
	@PutMapping(value = "/v1/meal/{mealId}")	
	public ResponseEntity<?> updateMeal(@PathVariable("mealId") long mealId , @RequestBody MealDTO mealDTO){
		if(mealsMethods.checkIfMealExists(mealId)) {
			mealDTO.setId(mealId);
			return ResponseEntity.ok(mealsMethods.update(mealDTO));
		}else {
			InfoDTO infoDTO = new InfoDTO();
			infoDTO.setDescription("The meal with id" + mealId + " doesn't exists");
			infoDTO.setStatus(HttpStatus.NOT_FOUND);
			return ResponseEntity.ok(infoDTO);
		}
	  
	}
	
}
