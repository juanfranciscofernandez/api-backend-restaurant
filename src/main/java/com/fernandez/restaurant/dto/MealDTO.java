package com.fernandez.restaurant.dto;

import java.util.List;

public class MealDTO {

	private long id;
	
	private String name;
	
	private List<MenuDTO> menuDTOList;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<MenuDTO> getMenuDTOList() {
		return menuDTOList;
	}

	public void setMenuDTOList(List<MenuDTO> menuDTOList) {
		this.menuDTOList = menuDTOList;
	}

	@Override
	public String toString() {
		return "MealDTO [id=" + id + ", name=" + name + ", menuDTOList=" + menuDTOList + "]";
	}
	
	
	
}
