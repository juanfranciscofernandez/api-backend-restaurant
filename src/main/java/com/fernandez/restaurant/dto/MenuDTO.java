package com.fernandez.restaurant.dto;

import java.util.ArrayList;

public class MenuDTO {
	
	private long id;
	private String name;
	private MealDTO mealDTO;
	
	private ArrayList<DishDTO> dishDTOList;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public MealDTO getMealDTO() {
		return mealDTO;
	}
	public void setMealDTO(MealDTO mealDTO) {
		this.mealDTO = mealDTO;
	}
	public ArrayList<DishDTO> getDishDTOList() {
		return dishDTOList;
	}
	public void setDishDTOList(ArrayList<DishDTO> dishDTOList) {
		this.dishDTOList = dishDTOList;
	}
	@Override
	public String toString() {
		return "MenuDTO [id=" + id + ", name=" + name + ", mealDTO=" + mealDTO + ", dishDTOList=" + dishDTOList + "]";
	}
	
	
	
	
	
}
