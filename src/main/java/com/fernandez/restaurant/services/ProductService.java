package com.fernandez.restaurant.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.restaurant.model.Product;
import com.fernandez.restaurant.repository.ProductRepository;

@Component
public class ProductService {

	@Autowired
	private ProductRepository productRepository;

	public List<Product> retreiveListOProducts() {
		return productRepository.findAll();
	}

	public Product create(Product product) {
		return productRepository.save(product);
	}

	public Product updateProduct(Product product) {
	    return productRepository.save(product);
	}
	
	public Product retreiveProduct(long productId) {
		return productRepository.getOne(productId);
	}

	public Optional<Product> checkIfProductExists(long productId) {
		return productRepository.findById(productId);
	}

	public void deleteById(long offerId) {
		productRepository.deleteById(offerId);
	}

}
