package com.fernandez.restaurant.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.restaurant.model.Meals;
import com.fernandez.restaurant.model.Offer;
import com.fernandez.restaurant.repository.MealRepository;

@Component
public class MealService {

	@Autowired
	private MealRepository mealRepository;

	public List<Meals> retreiveListMeals() {
		return mealRepository.findAll();
	}

	public Meals create(Meals meal) {
		return mealRepository.save(meal);
	}

	public Meals updateMeals(Meals offer) {
	    return mealRepository.save(offer);
	}
	
	public Meals retreiveMeals(long offerId) {
		return mealRepository.getOne(offerId);
	}

	public Optional<Meals> checkIfMealsExists(long offerId) {
		return mealRepository.findById(offerId);
	}

	public void deleteById(long offerId) {
		mealRepository.deleteById(offerId);
	}

}
