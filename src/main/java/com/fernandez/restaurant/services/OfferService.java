package com.fernandez.restaurant.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.restaurant.model.Offer;
import com.fernandez.restaurant.repository.OfferRepository;

@Component
public class OfferService {
	
	@Autowired
	private OfferRepository offerRepository;
			
	public List<Offer> retreiveListOffers(){
		return offerRepository.findAll();
	}
			
	public Offer create(Offer offer) {
	    return offerRepository.save(offer);
	}
	

	public Offer updateUser(Offer offer) {
	    return offerRepository.save(offer);
	}
	
	public Offer retreiveOffer(long offerId) {
		return offerRepository.getOne(offerId);
	}
	
	public void deleteById(long offerId) {
		offerRepository.deleteById(offerId);
	}
	
	public Optional<Offer> checkIfOfferExists(long offerId) {
		return offerRepository.findById(offerId);
	}
}


