package com.fernandez.restaurant.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.restaurant.model.Menu;
import com.fernandez.restaurant.repository.MenuRepository;

@Component
public class MenuService {
	
	@Autowired
	private MenuRepository menuRepository;

	
	public Menu retreiveMenu(long menuId) {
		return menuRepository.getOne(menuId);
	}
	
	public List<Menu> retreiveListMenu() {
		return menuRepository.findAll();
	}
	
	public Optional<Menu> checkIfMenuExists(long menuId) {
		return menuRepository.findById(menuId);
	}

}
