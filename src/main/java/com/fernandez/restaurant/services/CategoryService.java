package com.fernandez.restaurant.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.restaurant.model.Category;
import com.fernandez.restaurant.repository.CategoryRepository;

@Component
public class CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;

	public List<Category> retreiveListOfCategories() {
		return categoryRepository.findAll();
	}

	public Category create(Category category) {
		return categoryRepository.save(category);
	}

	public Category updateCategory(Category category) {
	    return categoryRepository.save(category);
	}
	
	public Category retreiveCategory(long categoryId) {
		return categoryRepository.getOne(categoryId);
	}

	public Optional<Category> checkIfCategoryExists(long categoryId) {
		return categoryRepository.findById(categoryId);
	}

	public void deleteById(long categoryId) {
		categoryRepository.deleteById(categoryId);
	}

}
