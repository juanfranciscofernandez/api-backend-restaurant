package com.fernandez.restaurant.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fernandez.restaurant.model.Dish;
import com.fernandez.restaurant.repository.DishesRepository;

@Component
public class DishesServices {

	@Autowired
	private DishesRepository dishesRepository;

	public List<Dish> retreiveListDishes() {
		return dishesRepository.findAll();
	}

	public Dish create(Dish dishes) {
		return dishesRepository.save(dishes);
	}

	public Dish update(Dish dishes) {
	    return dishesRepository.save(dishes);
	}
	
	public Dish retreive(long dishesId) {
		return dishesRepository.getOne(dishesId);
	}

	public Optional<Dish> checkIfDishesExists(long dishesId) {
		return dishesRepository.findById(dishesId);
	}

	public void deleteById(long dishesId) {
		dishesRepository.deleteById(dishesId);
	}

}
