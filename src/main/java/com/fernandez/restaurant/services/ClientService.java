package com.fernandez.restaurant.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fernandez.restaurant.model.Client;
import com.fernandez.restaurant.repository.ClientRepository;

@Service
public class ClientService {
	
	@Autowired
	private ClientRepository clientRepository;
	
	public List<Client> retreiveListOfClient() {
		return clientRepository.findAll();
	}
	
	public Client create(Client client) {
		return clientRepository.save(client);
	}
	
	public Client updateClient(Client client) {
	    return clientRepository.save(client);
	}
	
	public Client retreiveClient(long clientId) {
	    return clientRepository.getOne(clientId);
	}
	
	public Optional<Client> checkIfClientExists(long clientId) {
		return clientRepository.findById(clientId);
	}

	public void deleteById(long clientId) {
		clientRepository.deleteById(clientId);
	}
	
}
