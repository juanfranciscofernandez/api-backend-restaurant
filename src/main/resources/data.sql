insert into offers values(1,'Happy Hour');
insert into offers values(2,'Menu del día');
insert into offers values(3,'Menu noche');

insert into meals values(1,'Meal1');
insert into meals values(2,'Meal2');
insert into meals values(3,'Meal3');

insert into menu values(1,'Menu 1',1);
insert into menu values(2,'Menu 2',2);
insert into menu values(3,'Menu 3',1);
insert into menu values(4,'Menu 4',3);
insert into menu values(5,'Menu 5',1);
insert into menu values(6,'Menu 6',2);

insert into dishes (id,name,description) values(1,'Plato 1','Description plato 1');
insert into dishes (id,name,description) values(2,'Plato 2','Description plato 1');
insert into dishes (id,name,description) values(3,'Plato 3','Description plato 1');
insert into dishes (id,name,description) values(4,'Plato 4','Description plato 1');
insert into dishes (id,name,description) values(5,'Plato 5','Description plato 1');
insert into dishes (id,name,description) values(6,'Plato 6','Description plato 1');
insert into dishes (id,name,description) values(7,'Plato 7','Description plato 1');
insert into dishes (id,name,description) values(8,'Plato 8','Description plato 1');

insert into dishes_menu(dishes_id,menu_id) values(1,1);